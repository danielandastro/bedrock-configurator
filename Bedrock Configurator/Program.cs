﻿using System;
using Basic;
using Advanced;
namespace Bedrock_Configurator
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Welcome to Bedrock Configurator");
            Console.WriteLine("Would you like to configure advanced options as well");
            bool adv = Console.ReadLine().Contains("y");
            Basic.Basic.basic();
            if (adv) advanced.configAdvanced();
            else advanced.fillDefault();
            Console.WriteLine("All done, remember to copy the server.properties file into your server directory");
            Console.ReadKey();
        }
    }
}
