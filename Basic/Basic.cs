﻿using System;
using System.IO;
namespace Basic
{
    public static  class Basic
    {
        public static void basic()
        {
            string fileBuffer = "";
            File.Create("server.properties").Close();
            Console.WriteLine("Let's start with your server name, it can be anything, but be sensible: ");
            fileBuffer = "server-name=" + Console.ReadLine() + Environment.NewLine;
            Console.WriteLine("What should the game mode be set to, options are creative, survival or adventure: ");
            fileBuffer += "gamemode=" + Console.ReadLine() + Environment.NewLine;
            Console.WriteLine("Set the difficulty (peaceful, easy, normal, or hard)");
            fileBuffer += "difficulty=" + Console.ReadLine() + Environment.NewLine;
            Console.WriteLine("Would you like to allow cheat commands, type true for yes, or false for no");
            fileBuffer += "allow-cheats=" + Console.ReadLine() + Environment.NewLine;
            Console.WriteLine("How many players would you like to allow");
            fileBuffer += "max-players=" + Console.ReadLine() + Environment.NewLine;
            File.WriteAllText("server.properties", fileBuffer);
        }
    }
}
