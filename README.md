Bedrock configurator is a command line utility to configure minecraft bedrock servers
When run, you can either configure everything, or configure the basics and load optimized defaults for the advanced stuff

Please download the prebuilt files from releases

You may also build this yourself, from sources, simply open the sln file in Visual Studio (2019 tested), or Rider(untested), and execute the Bedrock Configurator project,
should you like to use the libraries, you are free to do so, as long as credit is given in any works published

I am one person developing, and I will try to sort out bugs, unfortunately, I am as of writing this, unable to test the MacOS and Ubuntu builds, so any bug reports, raised as issues
are welcome, and in fact encouraged, you may also message me on any platforms where this project is posted, such as reddit. Bug reports my be raised by emailing this address: incoming+danielandastro-bedrock-configurator-13152053-issue-@incoming.gitlab.com




You may download the server software from mojang [here](https://www.minecraft.net/en-us/download/server/bedrock/)


[![pipeline status](https://gitlab.com/danielandastro/bedrock-configurator/badges/master/pipeline.svg)](https://gitlab.com/danielandastro/bedrock-configurator/commits/master)