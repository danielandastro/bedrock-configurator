﻿using System;
using System.IO;
namespace Advanced
{
    public static class advanced
    {
        public static void fillDefault()
        {
            string fileBuffer = Environment.NewLine+"online-mode=true" 
                + Environment.NewLine + "white - list = false"
                + Environment.NewLine + "server-port=19132"
                + Environment.NewLine + "server-portv6=19133"
                + Environment.NewLine + "view-distance=32"
                + Environment.NewLine + "tick-distance=4"
                + Environment.NewLine + "player-idle-timeout=30"
                + Environment.NewLine + "max-threads=0"
                + Environment.NewLine + "level-name=Bedrock level"
                + Environment.NewLine + "level-seed="
                + Environment.NewLine + "default-player-permission-level=member"
                + Environment.NewLine + "texturepack-required=false";
        }
        public static void configAdvanced()
        {
            string fileBuffer = "";
            Console.WriteLine("Would you like to enforce xbox live checking");
            Console.WriteLine("You may not want this if you and your friends are not using official minecraft, but it" +
                " is a good idea if you are connecting through the internet");
            Console.WriteLine("Type true for yes, or false for no");
            fileBuffer += "online-mode=" + Console.ReadLine() + Environment.NewLine;

            Console.WriteLine("Would you like to enforce a whitelist, this prevents anyone who is not pre approved from using your server");
            Console.WriteLine("Type true for yes or false for no");
            fileBuffer += "white-list=" + Console.ReadLine() + Environment.NewLine;

            Console.WriteLine("Set the IPV4 server port, for default, type 19132");
            fileBuffer += "server-port=" + Console.ReadLine() + Environment.NewLine;
            Console.WriteLine("Set the IPV6 server port, for default, type 19133");
            fileBuffer += "server-portv6=" + Console.ReadLine() + Environment.NewLine;

            Console.WriteLine("Set the allowed view distance, for default, type 32");
            fileBuffer += "view-distance=" + Console.ReadLine() + Environment.NewLine;

            Console.WriteLine("Set the allowed tick distance, between 4-12, for default, type 4");
            fileBuffer += "tick-distance=" + Console.ReadLine() + Environment.NewLine;

            Console.WriteLine("How many minutes should the server wait before removing inactive players, to allow inactive players, type 0");
            fileBuffer += "player-idle-timeout" + Console.ReadLine() + Environment.NewLine;

            Console.WriteLine("How many CPU threads can the server use, if you don't know, set it to 0 to let minecraft figure it out");
            fileBuffer += "max-threads=" + Console.ReadLine() + Environment.NewLine;

            Console.WriteLine("What do you want to name the world, this is seperate from the server name, but be sensible");
            fileBuffer += "level-name=" + Console.ReadLine() + Environment.NewLine;

            Console.WriteLine("What seed should minecraft use to make the world, leave blank for random");
            fileBuffer += "level-seed=" + Console.ReadLine() + Environment.NewLine;

            Console.WriteLine("What permissions should new players have, we recommend either member or visitor, but you may set it to operator");
            fileBuffer += "default-player-permission-level=" + Console.ReadLine() + Environment.NewLine;

            Console.WriteLine("Should people be forced to use the server textures,  type true for yes or false for no");
            fileBuffer += "texturepack-require=" + Console.ReadLine() + Environment.NewLine;

            File.AppendAllText("server.properties", fileBuffer);
        }
    }
}
