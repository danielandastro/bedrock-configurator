To contribute code, please fork this repository

Make your code changes in a branch off the latest master branch

Create a pull request to merge back to this repository, with explanations of your code changes

The end